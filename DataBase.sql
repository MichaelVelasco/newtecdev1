-- Table: public.cities

-- DROP TABLE public.cities;

CREATE TABLE public.cities
(
    id integer NOT NULL GENERATED ALWAYS AS IDENTITY ( INCREMENT 1 START 1 MINVALUE 1 MAXVALUE 999999 CACHE 1 ),
    name character varying COLLATE pg_catalog."default" NOT NULL,
    CONSTRAINT cities_pkey PRIMARY KEY (id)
)

TABLESPACE pg_default;

ALTER TABLE public.cities
    OWNER to postgres;
	
-- Table: public.document_types

-- DROP TABLE public.document_types;

CREATE TABLE public.document_types
(
    id integer NOT NULL GENERATED ALWAYS AS IDENTITY ( INCREMENT 1 START 1 MINVALUE 1 MAXVALUE 9999 CACHE 1 ),
    name character varying COLLATE pg_catalog."default" NOT NULL,
    initials character varying COLLATE pg_catalog."default" NOT NULL DEFAULT 'NA'::character varying,
    CONSTRAINT document_type_pkey PRIMARY KEY (id)
)

TABLESPACE pg_default;

ALTER TABLE public.document_types
    OWNER to postgres;

-- Table: public.users

-- DROP TABLE public.users;

CREATE TABLE public.users
(
    id integer NOT NULL GENERATED ALWAYS AS IDENTITY ( INCREMENT 1 START 2 MINVALUE 1 MAXVALUE 99999 CACHE 1 ),
    name character varying COLLATE pg_catalog."default" NOT NULL,
	last_name character varying COLLATE pg_catalog."default" NOT NULL DEFAULT 'NA'::character varying,
	docuemnt_type integer NOT NULL DEFAULT 1,
    document_number integer NOT NULL DEFAULT 0,
    birthdate date NOT NULL DEFAULT CURRENT_DATE,
    city integer NOT NULL DEFAULT 1,
    CONSTRAINT users_pkey PRIMARY KEY (id),
    CONSTRAINT user_ciy_fk FOREIGN KEY (city)
        REFERENCES public.cities (id) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE CASCADE
        NOT VALID,
    CONSTRAINT user_document_type_fk FOREIGN KEY (docuemnt_type)
        REFERENCES public.document_types (id) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE CASCADE
        NOT VALID
)

TABLESPACE pg_default;

ALTER TABLE public.users
    OWNER to postgres;
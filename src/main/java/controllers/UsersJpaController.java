package Controllers;

import Controllers.exceptions.NonexistentEntityException;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import Models.Cities;
import Models.DocumentTypes;
import Models.Users;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

/**
 *
 * @author Grupo1
 */
public class UsersJpaController implements Serializable {

    public UsersJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Users users) {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Cities city = users.getCity();
            if (city != null) {
                city = em.getReference(city.getClass(), city.getId());
                users.setCity(city);
            }
            DocumentTypes docuemntType = users.getDocuemntType();
            if (docuemntType != null) {
                docuemntType = em.getReference(docuemntType.getClass(), docuemntType.getId());
                users.setDocuemntType(docuemntType);
            }
            em.persist(users);
            if (city != null) {
                city.getUsersCollection().add(users);
                city = em.merge(city);
            }
            if (docuemntType != null) {
                docuemntType.getUsersCollection().add(users);
                docuemntType = em.merge(docuemntType);
            }
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Users users) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Users persistentUsers = em.find(Users.class, users.getId());
            Cities cityOld = persistentUsers.getCity();
            Cities cityNew = users.getCity();
            DocumentTypes docuemntTypeOld = persistentUsers.getDocuemntType();
            DocumentTypes docuemntTypeNew = users.getDocuemntType();
            if (cityNew != null) {
                cityNew = em.getReference(cityNew.getClass(), cityNew.getId());
                users.setCity(cityNew);
            }
            if (docuemntTypeNew != null) {
                docuemntTypeNew = em.getReference(docuemntTypeNew.getClass(), docuemntTypeNew.getId());
                users.setDocuemntType(docuemntTypeNew);
            }
            users = em.merge(users);
            if (cityOld != null && !cityOld.equals(cityNew)) {
                cityOld.getUsersCollection().remove(users);
                cityOld = em.merge(cityOld);
            }
            if (cityNew != null && !cityNew.equals(cityOld)) {
                cityNew.getUsersCollection().add(users);
                cityNew = em.merge(cityNew);
            }
            if (docuemntTypeOld != null && !docuemntTypeOld.equals(docuemntTypeNew)) {
                docuemntTypeOld.getUsersCollection().remove(users);
                docuemntTypeOld = em.merge(docuemntTypeOld);
            }
            if (docuemntTypeNew != null && !docuemntTypeNew.equals(docuemntTypeOld)) {
                docuemntTypeNew.getUsersCollection().add(users);
                docuemntTypeNew = em.merge(docuemntTypeNew);
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = users.getId();
                if (findUsers(id) == null) {
                    throw new NonexistentEntityException("The users with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Users users;
            try {
                users = em.getReference(Users.class, id);
                users.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The users with id " + id + " no longer exists.", enfe);
            }
            Cities city = users.getCity();
            if (city != null) {
                city.getUsersCollection().remove(users);
                city = em.merge(city);
            }
            DocumentTypes docuemntType = users.getDocuemntType();
            if (docuemntType != null) {
                docuemntType.getUsersCollection().remove(users);
                docuemntType = em.merge(docuemntType);
            }
            em.remove(users);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Users> findUsersEntities() {
        return findUsersEntities(true, -1, -1);
    }

    public List<Users> findUsersEntities(int maxResults, int firstResult) {
        return findUsersEntities(false, maxResults, firstResult);
    }

    private List<Users> findUsersEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Users.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Users findUsers(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Users.class, id);
        } finally {
            em.close();
        }
    }

    public int getUsersCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Users> rt = cq.from(Users.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}

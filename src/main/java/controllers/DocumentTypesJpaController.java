package Controllers;

import Controllers.exceptions.IllegalOrphanException;
import Controllers.exceptions.NonexistentEntityException;
import Models.DocumentTypes;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import Models.Users;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

/**
 *
 * @author Grupo1
 */
public class DocumentTypesJpaController implements Serializable {

    public DocumentTypesJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(DocumentTypes documentTypes) {
        if (documentTypes.getUsersCollection() == null) {
            documentTypes.setUsersCollection(new ArrayList<Users>());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Collection<Users> attachedUsersCollection = new ArrayList<Users>();
            for (Users usersCollectionUsersToAttach : documentTypes.getUsersCollection()) {
                usersCollectionUsersToAttach = em.getReference(usersCollectionUsersToAttach.getClass(), usersCollectionUsersToAttach.getId());
                attachedUsersCollection.add(usersCollectionUsersToAttach);
            }
            documentTypes.setUsersCollection(attachedUsersCollection);
            em.persist(documentTypes);
            for (Users usersCollectionUsers : documentTypes.getUsersCollection()) {
                DocumentTypes oldDocuemntTypeOfUsersCollectionUsers = usersCollectionUsers.getDocuemntType();
                usersCollectionUsers.setDocuemntType(documentTypes);
                usersCollectionUsers = em.merge(usersCollectionUsers);
                if (oldDocuemntTypeOfUsersCollectionUsers != null) {
                    oldDocuemntTypeOfUsersCollectionUsers.getUsersCollection().remove(usersCollectionUsers);
                    oldDocuemntTypeOfUsersCollectionUsers = em.merge(oldDocuemntTypeOfUsersCollectionUsers);
                }
            }
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(DocumentTypes documentTypes) throws IllegalOrphanException, NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            DocumentTypes persistentDocumentTypes = em.find(DocumentTypes.class, documentTypes.getId());
            Collection<Users> usersCollectionOld = persistentDocumentTypes.getUsersCollection();
            Collection<Users> usersCollectionNew = documentTypes.getUsersCollection();
            List<String> illegalOrphanMessages = null;
            for (Users usersCollectionOldUsers : usersCollectionOld) {
                if (!usersCollectionNew.contains(usersCollectionOldUsers)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain Users " + usersCollectionOldUsers + " since its docuemntType field is not nullable.");
                }
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            Collection<Users> attachedUsersCollectionNew = new ArrayList<Users>();
            for (Users usersCollectionNewUsersToAttach : usersCollectionNew) {
                usersCollectionNewUsersToAttach = em.getReference(usersCollectionNewUsersToAttach.getClass(), usersCollectionNewUsersToAttach.getId());
                attachedUsersCollectionNew.add(usersCollectionNewUsersToAttach);
            }
            usersCollectionNew = attachedUsersCollectionNew;
            documentTypes.setUsersCollection(usersCollectionNew);
            documentTypes = em.merge(documentTypes);
            for (Users usersCollectionNewUsers : usersCollectionNew) {
                if (!usersCollectionOld.contains(usersCollectionNewUsers)) {
                    DocumentTypes oldDocuemntTypeOfUsersCollectionNewUsers = usersCollectionNewUsers.getDocuemntType();
                    usersCollectionNewUsers.setDocuemntType(documentTypes);
                    usersCollectionNewUsers = em.merge(usersCollectionNewUsers);
                    if (oldDocuemntTypeOfUsersCollectionNewUsers != null && !oldDocuemntTypeOfUsersCollectionNewUsers.equals(documentTypes)) {
                        oldDocuemntTypeOfUsersCollectionNewUsers.getUsersCollection().remove(usersCollectionNewUsers);
                        oldDocuemntTypeOfUsersCollectionNewUsers = em.merge(oldDocuemntTypeOfUsersCollectionNewUsers);
                    }
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = documentTypes.getId();
                if (findDocumentTypes(id) == null) {
                    throw new NonexistentEntityException("The documentTypes with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws IllegalOrphanException, NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            DocumentTypes documentTypes;
            try {
                documentTypes = em.getReference(DocumentTypes.class, id);
                documentTypes.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The documentTypes with id " + id + " no longer exists.", enfe);
            }
            List<String> illegalOrphanMessages = null;
            Collection<Users> usersCollectionOrphanCheck = documentTypes.getUsersCollection();
            for (Users usersCollectionOrphanCheckUsers : usersCollectionOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This DocumentTypes (" + documentTypes + ") cannot be destroyed since the Users " + usersCollectionOrphanCheckUsers + " in its usersCollection field has a non-nullable docuemntType field.");
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            em.remove(documentTypes);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<DocumentTypes> findDocumentTypesEntities() {
        return findDocumentTypesEntities(true, -1, -1);
    }

    public List<DocumentTypes> findDocumentTypesEntities(int maxResults, int firstResult) {
        return findDocumentTypesEntities(false, maxResults, firstResult);
    }

    private List<DocumentTypes> findDocumentTypesEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(DocumentTypes.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public DocumentTypes findDocumentTypes(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(DocumentTypes.class, id);
        } finally {
            em.close();
        }
    }

    public int getDocumentTypesCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<DocumentTypes> rt = cq.from(DocumentTypes.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Interfaces;

import java.util.List;

/**
 *
 * @author maico
 */
public interface Crud {
    public void create();
    public List read();
    public void update();
    public void delete();
    
    public boolean tableIsEmpty();
    public boolean tableIsNotEmpty();
}

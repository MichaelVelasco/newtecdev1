/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Views;

/**
 *
 * @author maico
 */
public class Menu {
    
    private final String[] items = {
        "Usuarios",
        "Ciudades",
        "Tipos de Documentos",
    };  
    
    public void showMenu(){
        System.out.println("\n**Bienvenido**\n");
        System.out.println("Elija una opción para gestionar:");
        for (int i = 0; i < this.items.length; i++) {
            System.out.println((i+1)+". "+items[i]+".");
        }
        System.out.println("0. Salir.");
    }
    
    public void exitMessage(){
        System.out.println("\nAdios!\n");
    }
    
    public void errorMessage(){
        System.out.println("\nOpción no válida.!\n");
    }
}

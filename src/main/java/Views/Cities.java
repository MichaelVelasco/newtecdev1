/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Views;

/**
 *
 * @author maico
 */
public class Cities {
    public void showMenu(){
        System.out.println("\nElija una opción para gestionar una ciudad:");
        System.out.println("1. Crear Ciudad.");
        System.out.println("2. Leer Ciudades.");
        System.out.println("3. Actualizar Ciudad.");
        System.out.println("4. Eliminar Ciudad.");
        
        this.excecuteOption(App.App.getScannerInt());
    }
    
    private void excecuteOption(int option){
        switch(option){
            case 1 -> new BusinessLogic.CitiesBusinessLogic().create();
            case 2 -> new BusinessLogic.CitiesBusinessLogic().readCities();
            case 3 -> new BusinessLogic.CitiesBusinessLogic().update();
            case 4 -> new BusinessLogic.CitiesBusinessLogic().delete();
            default -> this.showMenu();
        }
    }
}

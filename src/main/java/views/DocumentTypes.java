/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Views;

/**
 *
 * @author maico
 */
public class DocumentTypes {
    public void showMenu(){
        System.out.println("\nElija una opción para gestionar tipos de documentos:");
        System.out.println("1. Crear Tipo de Documento.");
        System.out.println("2. Leer Tipos de Documentos.");
        System.out.println("3. Actualizar Tipo de Documento.");
        System.out.println("4. Eliminar Tipo des Documento.");
        
        this.excecuteOption(App.App.getScannerInt());
    }
    
    private void excecuteOption(int option){
        switch(option){
            case 1 -> new BusinessLogic.DocumentTypesBusinessLogic().create();
            case 2 -> new BusinessLogic.DocumentTypesBusinessLogic().readDocumentTypes();
            case 3 -> new BusinessLogic.DocumentTypesBusinessLogic().update();
            case 4 -> new BusinessLogic.DocumentTypesBusinessLogic().delete();
            default -> this.showMenu();
        }
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Views;

/**
 *
 * @author maico
 */
public class Users {
    public void showMenu(){
        System.out.println("\nElija una opción para gestionar un usuario:");
        System.out.println("1. Crear Usuario.");
        System.out.println("2. Leer Usuarios.");
        System.out.println("3. Actualizar Usuario.");
        System.out.println("4. Eliminar Usuario.");
        
        this.excecuteOption(App.App.getScannerInt());
    }
    
    private void excecuteOption(int option){
        switch(option){
            case 1 -> new BusinessLogic.UsersBusinessLogic().create();
            case 2 -> new BusinessLogic.UsersBusinessLogic().readUsers();
            case 3 -> new BusinessLogic.UsersBusinessLogic().update();
            case 4 -> new BusinessLogic.UsersBusinessLogic().delete();
            default -> this.showMenu();
        }
    }
}

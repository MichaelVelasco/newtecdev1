package BusinessLogic;

import Controllers.CitiesJpaController;
import Controllers.exceptions.IllegalOrphanException;
import Controllers.exceptions.NonexistentEntityException;
import java.util.List;
import Models.Cities;

/**
 *
 * @author Grupo1
 */
public class CitiesBusinessLogic implements Interfaces.Crud{

    private final CitiesJpaController jpa = new CitiesJpaController(App.App.getEntityManagerFactory());
    
    @Override
    public void create() {
        System.out.println("Por favor ingrese los datos para crear una nueva ciudad:");
        Cities citie = new Cities();
        
        System.out.println("Nombre:");
        App.App.getScanner().nextLine();
        citie.setName(App.App.getScanner().nextLine());
        
        this.jpa.create(citie);
        System.out.println("Ciudad Creada.");
    }

    @Override
    public List<Cities> read() {
        return this.jpa.findCitiesEntities();
    }

    @Override
    public void update() {
        if(this.read().isEmpty()){
            System.out.println("No existen Ciudades.");
        }else{
            System.out.println("Por favor seleccione la ciudad a editar:");
            this.showCitiesById();
            Cities citie = this.getCitiById(App.App.getScannerInt(), true);

            try {
                System.out.println("Nombre:");
                App.App.getScanner().nextLine();
                citie.setName(App.App.getScanner().nextLine());   

                this.jpa.edit(citie);

                System.out.println("Ciudad Actualizada.");
            } catch (Exception ex) {
                System.out.println(ex);
            }
        }
    }

    @Override
    public void delete() {
        if(this.read().isEmpty()){
            System.out.println("No existen Ciudades.");
        }else{
            System.out.println("Por favor seleccione la ciudad a eliminar:");
            this.showCitiesById();
            try {
                this.jpa.destroy(App.App.getScannerInt());
                System.out.println("Ciudad eliminada.");
            } catch (IllegalOrphanException ex) {
                System.out.println("No es posible eliminar la ciudad.");
            } catch (NonexistentEntityException ex) {
                System.out.println("Ciudad no encontrada.");
                this.delete();
            }
        }
    }
    
    @Override
    public boolean tableIsEmpty() {
        return this.read().isEmpty();
    }

    @Override
    public boolean tableIsNotEmpty() {
        return !this.read().isEmpty();
    }
    
    public void readCities(){
        
        if(this.tableIsEmpty()){
            System.out.println("No existe Ciudades.");
        }else{
            this.showCitiesById();
            Cities citie = this.getCitiById(App.App.getScannerInt(), true);

            String userDataShow = "{\n";
            userDataShow += " Id = "+citie.getId()+"\n";
            userDataShow += " Name = "+citie.getName()+"\n";
            userDataShow += "}\n";

            System.out.print(userDataShow);
        }
    }
    
    public void showCitiesById(){
        System.out.println("\nCiudades:");
        List<Cities> cities = this.read();
        for (int i = 0; i < cities.size(); i++) {
            System.out.println(cities.get(i));
        }
    }
    
    public Cities getCitiById(int id, boolean notNull){
        
        Cities city = this.jpa.findCities(id);
        
        if(notNull && city == null){
            System.out.println("Ciudad con id "+id+" no encontrada.");
            this.showCitiesById();
            return this.getCitiById(App.App.getScannerInt(), notNull);
        }
        
        return city;
    }
    
    public CitiesJpaController getJpaController(){
        return this.jpa;
    }

}

package BusinessLogic;

import Controllers.UsersJpaController;
import Controllers.exceptions.NonexistentEntityException;
import java.util.List;
import Models.Users;

/**
 *
 * @author Grupo1
 */
public class UsersBusinessLogic implements Interfaces.Crud{

    private final UsersJpaController jpa = new UsersJpaController(App.App.getEntityManagerFactory());
    private final DocumentTypesBusinessLogic documentTypeLogic = new DocumentTypesBusinessLogic();
    private final CitiesBusinessLogic citiesLogic = new CitiesBusinessLogic();
    
    @Override
    public void create() {
        
        if(this.documentTypeLogic.tableIsEmpty()){
            System.out.println("No se puede crear un usuario debido a que no existen Tipos de Documentos.");
        }else if(this.citiesLogic.tableIsEmpty()){
            System.out.println("No se puede crear un usuario debido a que no existen Ciudades.");
        }else{
        
            System.out.println("Por favor ingrese los datos para crear un nuevo Usuario:");
            Users user = new Users();

            this.jpa.create(this.SetUserData(user));

            System.out.println("Usuario Creado.");
            
        }
    }

    @Override
    public List<Users> read() {
        return this.jpa.findUsersEntities();
    }

    @Override
    public void update() {
        System.out.println("Por favor seleccione el usuario a editar:");
        this.showUsersById();
        Users user = this.getUserById(App.App.getScannerInt(), true);
       
        try {
            this.jpa.edit(this.SetUserData(user));
            
            System.out.println("Usuario Actualizado.");
        } catch (Exception ex) {
            System.out.println(ex);
        }
    }

    @Override
    public void delete() {
        System.out.println("Por favor seleccione el usuario a eliminar:");
        this.showUsersById();
        try {
            this.jpa.destroy(App.App.getScannerInt());
            System.out.println("Usuario eliminado.");
        } catch (NonexistentEntityException ex) {
            System.out.println("Usuario no encontrado.");
            this.delete();
        }
    }
    
    @Override
    public boolean tableIsEmpty() {
        return this.read().isEmpty();
    }

    @Override
    public boolean tableIsNotEmpty() {
        return !this.read().isEmpty();
    }
    
    public Users SetUserData(Users user){
        
        System.out.println("\nNombres:");
        App.App.getScanner().nextLine();
        user.setName(App.App.getScanner().nextLine());

        System.out.println("\nApellidos:");
        user.setLastName(App.App.getScanner().nextLine());

        //seleccionar el Tipo de Documento
        this.documentTypeLogic.showDocumentTypesById();
        user.setDocuemntType(this.documentTypeLogic.getDocumentTypeById(App.App.getScannerInt(), true));

        System.out.println("\nNúmero de Documento:");
        user.setDocumentNumber(App.App.getScannerInt());

        //Fecha de Nacimiento
        System.out.println("\nFecha de Nacimiento:");
        user.setBirthdate(App.App.getScannerDate());

        //seleccionar la ciudad
        this.citiesLogic.showCitiesById();
        user.setCity(this.citiesLogic.getCitiById(App.App.getScannerInt(), true));
        
        return user;
    }
    
    public Users getUserById(int id, boolean notNull){
        
        Users user = this.jpa.findUsers(id);
        
        if(notNull && user == null){
            System.out.println("Usuario con id "+id+" no encontrado");
            this.showUsersById();
            return this.getUserById(App.App.getScannerInt(), notNull);
        }
        
        return user;
    }
    
    public void readUsers(){
        
        if(this.tableIsEmpty()){
            System.out.println("No existen usuarios.");
        }else{
            this.showUsersById();
            Users user = this.getUserById(App.App.getScannerInt(), true);

            String userDataShow = "{\n";
            userDataShow += " Id = "+user.getId()+"\n";
            userDataShow += " Name = "+user.getName()+"\n";
            userDataShow += " Last Name = "+user.getLastName()+"\n";
            userDataShow += " Document Type = "+user.getDocuemntType()+"\n";
            userDataShow += " Document Number = "+user.getDocumentNumber()+"\n";
            userDataShow += " Birthdate = "+user.getBirthdate()+"\n";
            userDataShow += " city = "+user.getCity()+"\n";
            userDataShow += "}\n";

            System.out.print(userDataShow);
        }   
    }
    
    public void showUsersById(){
        System.out.println("\nUsusarios:");
        List<Users> users = this.read();
        for (int i = 0; i < users.size(); i++) {
            System.out.println(users.get(i));
        }
    }

    public UsersJpaController getJpaController(){
        return this.jpa;
    }
    
}

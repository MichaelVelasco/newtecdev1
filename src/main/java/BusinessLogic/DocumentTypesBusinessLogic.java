package BusinessLogic;

import Controllers.DocumentTypesJpaController;
import Controllers.exceptions.IllegalOrphanException;
import Controllers.exceptions.NonexistentEntityException;
import java.util.List;
import Models.DocumentTypes;

/**
 *
 * @author Grupo1
 */
public class DocumentTypesBusinessLogic implements Interfaces.Crud{

    private final DocumentTypesJpaController jpa = new DocumentTypesJpaController(App.App.getEntityManagerFactory());
    
    @Override
    public void create() {
        System.out.println("Por favor ingrese los datos para crear un nuevo tipo de documento:");
        DocumentTypes documentType = new DocumentTypes();
        
        System.out.println("Nombre:");
        App.App.getScanner().nextLine();
        documentType.setName(App.App.getScanner().nextLine());
        
        System.out.println("\nSiglas:");
        documentType.setInitials(App.App.getScanner().nextLine());
        
        this.jpa.create(documentType);
        
        System.out.println("Tipo de Documento creado.");
    }

    @Override
    public List<DocumentTypes> read() {
        return this.jpa.findDocumentTypesEntities();
    }

    @Override
    public void update() {
        if(this.read().isEmpty()){
            System.out.println("No existen Tipos de Documentos.");
        }else{
            System.out.println("Por favor seleccione el Tipo de Documento a editar:");
            this.showDocumentTypesById();
            DocumentTypes documentType = this.getDocumentTypeById(App.App.getScannerInt(), true);

            try {
                System.out.println("Nombre:");
                App.App.getScanner().nextLine();
                documentType.setName(App.App.getScanner().nextLine());

                System.out.println("Siglas:");
                documentType.setInitials(App.App.getScanner().nextLine());

                this.jpa.edit(documentType);

                System.out.println("Tipo de Documento Actualizado.");
            } catch (Exception ex) {
                System.out.println(ex);
            }
        }
    }

    @Override
    public void delete() {
        if(this.read().isEmpty()){
            System.out.println("No existen Tipos de Documentos.");
        }else{
            System.out.println("Por favor seleccione el Tipo de Documento a eliminar:");
            this.showDocumentTypesById();
            try {
                this.jpa.destroy(App.App.getScannerInt());
                System.out.println("Tipo de Documento eliminado.");
            } catch (IllegalOrphanException ex) {
                System.out.println("No es posible eliminar el Tipo de Documento.");
            } catch (NonexistentEntityException ex) {
                System.out.println("Tipo de Documento no encontrado.");
                this.delete();
            }
        }
    }
    
    @Override
    public boolean tableIsEmpty() {
        return this.read().isEmpty();
    }

    @Override
    public boolean tableIsNotEmpty() {
        return !this.read().isEmpty();
    }
    
    public void readDocumentTypes(){
        
        if(this.tableIsEmpty()){
            System.out.println("No existen Tipos de Documentos.");
        }else{
            this.showDocumentTypesById();
            DocumentTypes documentType = this.getDocumentTypeById(App.App.getScannerInt(), true);

            String userDataShow = "{\n";
            userDataShow += " Id = "+documentType.getId()+"\n";
            userDataShow += " Name = "+documentType.getName()+"\n";
            userDataShow += " Initials = "+documentType.getInitials()+"\n";
            userDataShow += "}\n";

            System.out.print(userDataShow);
        }
    }
    
    public void showDocumentTypesById(){
        System.out.println("\nTipos de Documentos:");
        List<DocumentTypes> documentTypes = this.read();
        for (int i = 0; i < documentTypes.size(); i++) {
            System.out.println(documentTypes.get(i));
        }
    }
    
    public DocumentTypes getDocumentTypeById(int id, boolean notNull){
        
        DocumentTypes documentType = this.jpa.findDocumentTypes(id);
        
        if(notNull && documentType == null){
            System.out.println("Tipo de Documento con id "+id+" no encontrado.");
            this.showDocumentTypesById();
            return this.getDocumentTypeById(App.App.getScannerInt(), notNull);
        }
        
        return documentType;
    }
    
    public DocumentTypesJpaController getJpaController(){
        return this.jpa;
    }

}

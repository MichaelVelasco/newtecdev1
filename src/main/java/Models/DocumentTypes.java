package Models;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 *
 * @author Grupo1
 */
@Entity
@Table(name = "document_types", catalog = "postgres", schema = "public")
@NamedQueries({
    @NamedQuery(name = "DocumentTypes.findAll", query = "SELECT d FROM DocumentTypes d"),
    @NamedQuery(name = "DocumentTypes.findById", query = "SELECT d FROM DocumentTypes d WHERE d.id = :id"),
    @NamedQuery(name = "DocumentTypes.findByName", query = "SELECT d FROM DocumentTypes d WHERE d.name = :name"),
    @NamedQuery(name = "DocumentTypes.findByInitials", query = "SELECT d FROM DocumentTypes d WHERE d.initials = :initials")})
public class DocumentTypes implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(nullable = false)
    private Integer id;
    @Basic(optional = false)
    @Column(nullable = false, length = 2147483647)
    private String name;
    @Basic(optional = false)
    @Column(nullable = false, length = 2147483647)
    private String initials;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "docuemntType")
    private Collection<Users> usersCollection;

    public DocumentTypes() {
    }

    public DocumentTypes(Integer id) {
        this.id = id;
    }

    public DocumentTypes(Integer id, String name, String initials) {
        this.id = id;
        this.name = name;
        this.initials = initials;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getInitials() {
        return initials;
    }

    public void setInitials(String initials) {
        this.initials = initials;
    }

    public Collection<Users> getUsersCollection() {
        return usersCollection;
    }

    public void setUsersCollection(Collection<Users> usersCollection) {
        this.usersCollection = usersCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof DocumentTypes)) {
            return false;
        }
        DocumentTypes other = (DocumentTypes) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return this.id+". "+this.name;
    }
    
}

package Services;

import Controllers.CitiesJpaController;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 *
 * @author Grupo1
 */
@RestController()
@RequestMapping(path = "/cities")
public class Cities{
    
    private CitiesJpaController cities = new CitiesJpaController(App.App.getEntityManagerFactory());
    
    @PostMapping("/create")
    public Response create(@RequestParam String name) {
        Models.Cities city = new Models.Cities(null, name);
        try {
            this.cities.create(city);
        } catch (Exception e) {
            return new Response(false, "Ocurrió un error al intentar crear la ciudad: "+e.getMessage());
        }
        return new Response(true, "Ciudad creada exitosamente.", new CityResponse(city));
    }
    
    @GetMapping("/read/{id}")
    public Response read(@PathVariable int id){
        Models.Cities city = this.cities.findCities(id);
        if(city == null){
            return new Response(false, "No se encontraron ciudades con id: "+id);
        }
        return new Response(true, "Ciudad encontrada.", new CityResponse(city));
    }
    
    @GetMapping("/read")
    public Response read(){
        List<Models.Cities> citites = this.cities.findCitiesEntities();
        if(citites.size() <= 0){
            return new Response(false, "No se encontraron ciudades.");
        } 
        return new Response(true, "Ciudades encontradas", this.listCityResponse(citites));
    }

    @PutMapping("/update/{id}")
    public Response update(@PathVariable int id, @RequestParam String name) {
        Models.Cities city = this.cities.findCities(id);
        if(city == null){
            return new Response(false, "No se encontraron ciudades con id: "+id);
        }
        try {
            city.setName(name);
            this.cities.edit(city);
        } catch (Exception e) {
            return new Response(false, "Ocurrió un error al editar la ciudad: "+e.getMessage());
        }
        
        return new Response(true, "Ciudad Actualizada exitosamente.", new CityResponse(city));
    }

    @DeleteMapping("/delete/{id}")
    public Response delete(@PathVariable int id) {
        Models.Cities city = this.cities.findCities(id);
        if(city == null){
            return new Response(false, "No se encontraron ciudades con id: "+id);
        }
        try {
            this.cities.destroy(id);
        } catch (Exception e) {
            return new Response(false, "Ocurrió un error al intentar eliminar la ciudad: "+e.getMessage());
        }
        return new Response(true, "Ciudad Eliminada.", new CityResponse(city));
    }
    
    public static class CityResponse implements Serializable{
        
        public int id;
        public String name;

        public CityResponse(Models.Cities city) {
            this.id = city.getId();
            this.name = city.getName();
        }
        
    }
    
    private List<Serializable> listCityResponse(List<Models.Cities> citites){
        List<Serializable> list = new ArrayList<>();
        citites.forEach(city -> {
            list.add(new CityResponse(city));
        });
        return list;
    }
    
}

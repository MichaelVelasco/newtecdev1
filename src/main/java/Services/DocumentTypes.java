package Services;

import Controllers.DocumentTypesJpaController;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Grupo1
 */
@RestController()
@RequestMapping(path = "/documentTypes")
public class DocumentTypes{
    
    private DocumentTypesJpaController documentTypes = new DocumentTypesJpaController(App.App.getEntityManagerFactory());

    @PostMapping("/create")
    public Response create(@RequestParam String name, @RequestParam String initials) {
        Models.DocumentTypes documentType = new Models.DocumentTypes(null, name, initials);
        try {
            this.documentTypes.create(documentType);
        } catch (Exception e) {
            return new Response(false, "Ocurrió un error al intentar crear el tipo de documento: "+e.getMessage());
        }
        return new Response(true, "Tipo de documento creado exitosamente.", new DocumentTypeResponse(documentType));
    }

    @GetMapping("/read")
    public Response read() {
        List<Models.DocumentTypes> documentTypes = this.documentTypes.findDocumentTypesEntities();
        if(documentTypes.size() <= 0){
            return new Response(false, "No se encontraron tipos de documentos.");
        } 
        return new Response(true, "tipos de documentos encontrados", this.listDocumentTypeResponse(documentTypes));
    }

    @GetMapping("/read/{id}")
    public Response read(@PathVariable int id) {
        Models.DocumentTypes documentType = this.documentTypes.findDocumentTypes(id);
        if(documentTypes == null){
            return new Response(false, "No se encontraron tipos de documentos con id: "+id);
        } 
        return new Response(true, "tipo de documento encontrado.", new DocumentTypeResponse(documentType));
    }
    
    @PutMapping("/update/{id}")
    public Response update(@PathVariable int id, @RequestParam String name, @RequestParam String initials) {
        Models.DocumentTypes documentType = this.documentTypes.findDocumentTypes(id);
        if(documentTypes == null){
            return new Response(false, "No se encontraron tipos de documentos con id: "+id);
        }
        try {
            documentType.setName(name);
            documentType.setInitials(initials);
            this.documentTypes.edit(documentType);
        } catch (Exception e) {
            return new Response(false, "Ocurrió un error al editar el tipo de documento: "+e.getMessage());
        }
        return new Response(true, "Tipo de Documento actualizado exitosamente.", new DocumentTypeResponse(documentType));
    }

    @DeleteMapping("/delete/{id}")
    public Response delete(@PathVariable int id) {
        Models.DocumentTypes documentType = this.documentTypes.findDocumentTypes(id);
        if(documentTypes == null){
            return new Response(false, "No se encontraron tipos de documentos con id: "+id);
        }
        try {
            this.documentTypes.destroy(id);
        } catch (Exception e) {
            return new Response(false, "Ocurrió un error al eliminar el tipo de documento: "+e.getMessage());
        }
        return new Response(false, "Tipo de Documento eliminado exitosamente.", new DocumentTypeResponse(documentType));
    }
    
    public static class DocumentTypeResponse implements Serializable{
        
        public int id;
        public String name;
        public String initials;

        public DocumentTypeResponse(Models.DocumentTypes documentType) {
            this.id = documentType.getId();
            this.name = documentType.getName();
            this.initials = documentType.getInitials();
        }
    }
    
    private List<Serializable> listDocumentTypeResponse(List<Models.DocumentTypes> documentTypes){
        List<Serializable> list = new ArrayList<>();
        documentTypes.forEach(documentType -> {
            list.add(new DocumentTypes.DocumentTypeResponse(documentType));
        });
        return list;
    }
    
}

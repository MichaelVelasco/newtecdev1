/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Services;

import Controllers.CitiesJpaController;
import Controllers.DocumentTypesJpaController;
import Controllers.UsersJpaController;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author maico
 */
@RestController()
@RequestMapping(path = "/users")
public class Users{
    
    private UsersJpaController users = new UsersJpaController(App.App.getEntityManagerFactory());
    private DocumentTypesJpaController documentTypes = new DocumentTypesJpaController(App.App.getEntityManagerFactory());
    private CitiesJpaController cities = new CitiesJpaController(App.App.getEntityManagerFactory());

    @GetMapping("/read")
    public Response read() {
        List<Models.Users> users = this.users.findUsersEntities();
        if(users.size() <= 0){
            return new Response(false, "No se encontraron usuarios.");
        } 
        return new Response(true, "Usuarios encontrados", this.listUserResponse(users));
    }

    @GetMapping("/read/{id}")
    public Response read(@PathVariable int id) {
        Models.Users user = this.users.findUsers(id);
        if(user == null){
            return new Response(false, "No se encontraron usuarios con id: "+id);
        }
        return new Response(true, "Usuario encontrado.", new UserResponse(user));
    }

    @PostMapping("/create")
    public Response create(@RequestParam String name, @RequestParam String lastName, @RequestParam int documentTypeId, @RequestParam int documentNumber, @RequestParam Date birthdate, @RequestParam int cityId) {
        Models.Users user = new Models.Users(null, name, lastName, documentNumber, birthdate);
        Models.DocumentTypes documentType = this.documentTypes.findDocumentTypes(documentTypeId);
        Models.Cities city = this.cities.findCities(cityId);
        
        if(documentType == null){
            return new Response(false, "No se encontró Tipo de docuemnto con id: "+documentTypeId);
        }
        
        if(city == null){
            return new Response(false, "No se encontró Ciudad con id: "+cityId);
        }
        
        try {
            user.setDocuemntType(documentType);
            user.setCity(city);
            this.users.create(user);
        } catch (Exception e) {
            return new Response(false, "Ocurrió un error al intentar crear el usuario: "+e.getMessage());
        }
        
        return new Response(true, "Usuario creado exitosamente", new UserResponse(user));
    }
    
    @PutMapping("/update/{id}")
    public Response update(
            @PathVariable int id,
            @RequestParam(required = false) String name,
            @RequestParam(required = false) String lastName,
            @RequestParam(required = false, defaultValue = "-1") int documentTypeId,
            @RequestParam(required = false, defaultValue = "-1") int documentNumber,
            @RequestParam(required = false) Date birthdate,
            @RequestParam(required = false, defaultValue = "-1") int cityId)
    { 
        Models.Users user = this.users.findUsers(id);
        Models.DocumentTypes documentType;
        Models.Cities city;
        
        if(user == null){
            return new Response(false, "No se encontraron usuarios con Id: "+id);
        }
        
        if(name != null){
            user.setName(name);
        }
        
        if(lastName != null){
            user.setLastName(lastName);
        }
        
        if(documentTypeId != -1){
            documentType = this.documentTypes.findDocumentTypes(documentTypeId);
            if(documentType != null){
                user.setDocuemntType(documentType);
            }else{
                return new Response(false, "No se encontró Tipo de docuemnto con id: "+documentTypeId);
            }
        }
        
        if(documentNumber != -1){
            user.setDocumentNumber(documentNumber);
        }
        
        if(birthdate != null){
            user.setBirthdate(birthdate);
        }
        
        if(cityId != -1){
            city = this.cities.findCities(cityId);
            if(city != null){
                user.setCity(city);
            }else{
                return new Response(false, "No se encontró Ciudad con id: "+cityId);
            }
        }
        
        try {
            this.users.edit(user);
        } catch (Exception e) {
            return new Response(false, "Ocurrió un error al intentar actualizart el usuario: "+e.getMessage());
        }
        
        return new Response(true, "Usuario actualizado exitosamente", new UserResponse(user));
    }
    
    @DeleteMapping("/delete/{id}")
    public Response delete(@PathVariable int id) {
        Models.Users user = this.users.findUsers(id);
        if(user == null){
            return new Response(false, "No se encontraron usuarios con id: "+id);
        }
        try {
            this.users.destroy(id);
        } catch (Exception e) {
            return new Response(false, "Ocurrió un error al intentar eliminar el usuario: "+e.getMessage());
        }
        return new Response(true, "Usuario Eliminado.", new Users.UserResponse(user));
    }
    
    public class UserResponse implements Serializable{
        
        public int id;
        public String name;
        public String lastName;
        public Services.DocumentTypes.DocumentTypeResponse documentType;
        public int documentNumber;
        public Date birthdate;
        public Services.Cities.CityResponse city;

        public UserResponse(Models.Users user) {
            this.id = user.getId();
            this.name = user.getName();
            this.lastName = user.getLastName();
            this.documentType = new Services.DocumentTypes.DocumentTypeResponse(user.getDocuemntType());
            this.documentNumber = user.getDocumentNumber();
            this.birthdate = user.getBirthdate();
            this.city = new Services.Cities.CityResponse(user.getCity());
        }
        
    }
    
    private List<Serializable> listUserResponse(List<Models.Users> users){
        List<Serializable> list = new ArrayList<>();
        users.forEach(user -> {
            list.add(new Users.UserResponse(user));
        });
        return list;
    }
    
}

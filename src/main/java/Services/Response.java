package Services;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Grupo1
 */
public class Response implements Serializable{
    
    public boolean success;
    public String message;
    public List<Serializable> objects;

    public Response(boolean success, String message, List<Serializable> objects) {
        this.success = success;
        this.message = message;
        this.objects = objects;
    }
    
    public Response(boolean success, String message, Serializable object) {
        this.success = success;
        this.message = message;
        this.objects = new ArrayList<>();
        this.objects.add(object);
    }
    
    public Response(boolean success, String message) {
        this.success = success;
        this.message = message;
        this.objects = new ArrayList<>();
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<Serializable> getObjects() {
        return objects;
    }

    public void setObjects(List<Serializable> objects) {
        this.objects = objects;
    }
    
    
}

package App;

import java.util.Scanner;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import Views.Menu;
import java.util.Date;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 *
 * @author Grupo1
 */
@SpringBootApplication
public class App {
    
    private static final Menu menu = new Menu();
    private static boolean exit = false;
    private static final EntityManagerFactory emf = Persistence.createEntityManagerFactory("newTecDev_NewTecDev1_jar_1.0-SNAPSHOTPU");
    private static Scanner scanner = new Scanner(System.in);
    
    public static void main(String[] args) {
        initSpringServices(args);
        while (!exit) {
            try {
                menu.showMenu();
                excecuteOption(getScannerInt());
            } catch (Exception e) {
                System.out.println("Algo Salió mal!");
                exit = true;
            }
        }
    }
    
    private static void excecuteOption(int option){   
        switch(option){
            case 1 -> new Views.Users().showMenu();
            case 2 -> new Views.Cities().showMenu();
            case 3 -> new Views.DocumentTypes().showMenu();
            case 0 -> {
                exit = true;
                menu.exitMessage();
            }
            default -> menu.errorMessage();
        }
    }
    
    public static Scanner getScanner(){
        return scanner;
    }
    
    public static int getScannerInt(){
    
        String number;
        do {            
            try {
                number = scanner.next();
                if(isNumber(number)){
                    break;
                }else{
                    throw new Exception();
                }
            } catch (Exception e) {
                System.out.println("Por favor ingrese un número, sin letras ni espacios.");
            }
        } while (true);
        
        return Integer.parseInt(number);
    }
    
    public static boolean isNumber(String str){
        return str.matches("-?\\d+(\\.\\d+)?");
    }
    
    public static Date getScannerDate(){
        
        System.out.println("Año (1901 en adelante):");
        int year = getScannerInt();
        year = year-1900;
        while (!(year >= 1)) { 
            System.out.println("Escoja un a partir de 1901 en adelante");
            year = getScannerInt();
            year = year-1900;    
        }

        System.out.println("Mes (1 - 12):");          
        int month = getScannerInt();
        month--;
        while (!(month >= 0 && month <= 11)){
            System.out.println("Escoja un número entre 1 y 12");
            month = getScannerInt();
            month--;
        }

        System.out.println("Día (1 - 31):");
        int day = getScannerInt();
        while (!(day >= 1 && day <= 31)){
            System.out.println("Escoja un número entre 1 y 31");
            day = getScannerInt();
        }
        
        return new Date(year, month, day);
        
    }
    
    public static EntityManagerFactory getEntityManagerFactory(){
        return emf;
    }
    
    private static void initSpringServices(String[] args){
        Class[] classes = {
            App.class,
            Services.Cities.class,
            Services.DocumentTypes.class,
            Services.Users.class
        };
        SpringApplication.run(classes, args);
    }
}

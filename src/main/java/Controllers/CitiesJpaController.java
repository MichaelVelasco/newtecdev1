package Controllers;

import Controllers.exceptions.IllegalOrphanException;
import Controllers.exceptions.NonexistentEntityException;
import Models.Cities;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import Models.Users;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

/**
 *
 * @author Grupo1
 */
public class CitiesJpaController implements Serializable {

    public CitiesJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Cities cities) {
        if (cities.getUsersCollection() == null) {
            cities.setUsersCollection(new ArrayList<Users>());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Collection<Users> attachedUsersCollection = new ArrayList<Users>();
            for (Users usersCollectionUsersToAttach : cities.getUsersCollection()) {
                usersCollectionUsersToAttach = em.getReference(usersCollectionUsersToAttach.getClass(), usersCollectionUsersToAttach.getId());
                attachedUsersCollection.add(usersCollectionUsersToAttach);
            }
            cities.setUsersCollection(attachedUsersCollection);
            em.persist(cities);
            for (Users usersCollectionUsers : cities.getUsersCollection()) {
                Cities oldCityOfUsersCollectionUsers = usersCollectionUsers.getCity();
                usersCollectionUsers.setCity(cities);
                usersCollectionUsers = em.merge(usersCollectionUsers);
                if (oldCityOfUsersCollectionUsers != null) {
                    oldCityOfUsersCollectionUsers.getUsersCollection().remove(usersCollectionUsers);
                    oldCityOfUsersCollectionUsers = em.merge(oldCityOfUsersCollectionUsers);
                }
            }
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Cities cities) throws IllegalOrphanException, NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Cities persistentCities = em.find(Cities.class, cities.getId());
            Collection<Users> usersCollectionOld = persistentCities.getUsersCollection();
            Collection<Users> usersCollectionNew = cities.getUsersCollection();
            List<String> illegalOrphanMessages = null;
            for (Users usersCollectionOldUsers : usersCollectionOld) {
                if (!usersCollectionNew.contains(usersCollectionOldUsers)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain Users " + usersCollectionOldUsers + " since its city field is not nullable.");
                }
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            Collection<Users> attachedUsersCollectionNew = new ArrayList<Users>();
            for (Users usersCollectionNewUsersToAttach : usersCollectionNew) {
                usersCollectionNewUsersToAttach = em.getReference(usersCollectionNewUsersToAttach.getClass(), usersCollectionNewUsersToAttach.getId());
                attachedUsersCollectionNew.add(usersCollectionNewUsersToAttach);
            }
            usersCollectionNew = attachedUsersCollectionNew;
            cities.setUsersCollection(usersCollectionNew);
            cities = em.merge(cities);
            for (Users usersCollectionNewUsers : usersCollectionNew) {
                if (!usersCollectionOld.contains(usersCollectionNewUsers)) {
                    Cities oldCityOfUsersCollectionNewUsers = usersCollectionNewUsers.getCity();
                    usersCollectionNewUsers.setCity(cities);
                    usersCollectionNewUsers = em.merge(usersCollectionNewUsers);
                    if (oldCityOfUsersCollectionNewUsers != null && !oldCityOfUsersCollectionNewUsers.equals(cities)) {
                        oldCityOfUsersCollectionNewUsers.getUsersCollection().remove(usersCollectionNewUsers);
                        oldCityOfUsersCollectionNewUsers = em.merge(oldCityOfUsersCollectionNewUsers);
                    }
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = cities.getId();
                if (findCities(id) == null) {
                    throw new NonexistentEntityException("The cities with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws IllegalOrphanException, NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Cities cities;
            try {
                cities = em.getReference(Cities.class, id);
                cities.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The cities with id " + id + " no longer exists.", enfe);
            }
            List<String> illegalOrphanMessages = null;
            Collection<Users> usersCollectionOrphanCheck = cities.getUsersCollection();
            for (Users usersCollectionOrphanCheckUsers : usersCollectionOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Cities (" + cities + ") cannot be destroyed since the Users " + usersCollectionOrphanCheckUsers + " in its usersCollection field has a non-nullable city field.");
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            em.remove(cities);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Cities> findCitiesEntities() {
        return findCitiesEntities(true, -1, -1);
    }

    public List<Cities> findCitiesEntities(int maxResults, int firstResult) {
        return findCitiesEntities(false, maxResults, firstResult);
    }

    private List<Cities> findCitiesEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Cities.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Cities findCities(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Cities.class, id);
        } finally {
            em.close();
        }
    }

    public int getCitiesCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Cities> rt = cq.from(Cities.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
